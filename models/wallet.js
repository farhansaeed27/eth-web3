const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const walletSchema = new Schema({
  userId: { type: Schema.Types.ObjectId, ref: "users" },
  address: { type: String, required: true },
  balance: { type: Number, default: 0 }
});
module.exports = mongoose.model("wallets", walletSchema);
