pragma solidity ^0.4.22;

contract userDeposit {
    address public owner=msg.sender;
    
    event received(address from, uint256 value, uint256 blocknumber);

    function () payable public{
    emit received(msg.sender, msg.value, block.number);
    }
    function withdraw(){
        owner.transfer(address(this).balance);
    }
}