const { celebrate, Joi, Segments } = require("celebrate");
module.exports = {
  validateSignup() {
    return celebrate({
      [Segments.BODY]: Joi.object().keys({
        email: Joi.string()
          .email()
          .required(),
        password: Joi.string()
          .min(6)
          .required()
      })
    });
  },
  validateLogin() {
    return celebrate({
      [Segments.BODY]: Joi.object().keys({
        email: Joi.string()
          .email()
          .required(),
        password: Joi.string()
          .min(6)
          .required()
      })
    });
  }
};
