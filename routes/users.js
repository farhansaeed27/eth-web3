var express = require("express");
var router = express.Router();
const userSchema = require("../models/user");
const adminSchema = require("../models/admin");
const walletSchema = require("../models/wallet");
const { validateSignup, validateLogin } = require("../validation");
const web3 = require("../utils/web3");
const {
  interface, //get contract interface to deploy the contract
  bytecode //get the bytecode of contract for deployment
} = require("../ethereum/compile");

function deploy(interface, bytecode) {
  return new Promise(async (res, rej) => {
    let accounts = await web3.eth.getAccounts();
    let account = accounts[0];
    console.log(await web3.eth.getBalance(account));
    let contract = await new web3.eth.Contract(JSON.parse(interface))
      .deploy({
        data: "0x" + bytecode
      })
      .send({ from: account, gas: "2000000" });
    res(contract.options.address);
  });
}

router.get("/", function(req, res, next) {
  res.send("respond with a resource");
});
router.post("/login", validateSignup(), (req, res) => {
  let { email, password } = req.body;
  userSchema
    .findByCredentials(email, password)
    .then(user => {
      return user.generateAuthToken().then(token => {
        res.header("x-auth", token).send(user);
      });
    })
    .catch(e => {
      console.log(e);
      res.status(400).send({ error: { message: e } });
    });
});
router.post("/send", async (req, res) => {
  let { from, to, value } = req.body;
  let accounts = await web3.eth.getAccounts();
  let fromWallet = await walletSchema.findOne({ userId: from });

  let toWallet = await walletSchema.findOne({ userId: to });
  if (fromWallet.balance < value) {
    return res.status(500).json({ error: { message: "Insufficiant Balance" } });
  }
  web3.eth.getAccounts().then(async accounts => {
    web3.eth
      .sendTransaction({
        from: accounts[0],
        to: toWallet.address,
        value: await web3.utils.toWei(value, "ether")
      })
      .then(receipt => {
        res.json(receipt);
      });
  });
});
router.post("/signup", validateSignup(), (req, res) => {
  let { email, password } = req.body;
  let newUser = new userSchema({ email, password });
  newUser.save().then(user => {
    deploy(interface, bytecode).then(address => {
      let newWallet = new walletSchema({
        userId: user._id,
        address
      });
      newWallet.save().then(wallet => {
        user.generateAuthToken().then(token => {
          res.status(200).json({ token });
        });
      });
    });
  });
});

module.exports = router;
