var express = require("express");
var router = express.Router();
const web3 = require("../utils/web3");
/* GET home page. */
router.get("/", function(req, res, next) {
  res.render("index", { title: "Express" });
});
router.get("/getBalance", (req, res) => {
  web3.eth.getAccounts().then(async accounts => {
    let data = [];
    for (const account of accounts) {
      data.push({
        account,
        balance:
          (await web3.utils.fromWei(
            await web3.eth.getBalance(account),
            "ether"
          )) + " ether"
      });
    }
    res.json({ data });
  });
});
module.exports = router;
